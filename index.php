<?php
include_once "./config/connection.php";
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Duvan Playstation Admin</title>
    <link rel="stylesheet" type="text/css" href="index.css">
</head>

<body>
    <div class="Kontener">
        <div class="navigasi">
            <ul>
                <li>
                    <a href="#">
                        <span class="icon">
                            <ion-icon name="game-controller-outline"></ion-icon>
                        </span>
                        <span class="title">Duvan Playstation</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="icon">
                            <ion-icon name="home-outline"></ion-icon>
                        </span>
                        <span class="title">Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="icon">
                            <ion-icon name="people-outline"></ion-icon>
                        </span>
                        <span class="title">Customers</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="icon">
                            <ion-icon name="chatbubble-ellipses-outline"></ion-icon>
                        </span>
                        <span class="title">Message</span>
                    </a>
                </li>
                <li>
                    <a href="register.html">
                        <span class="icon">
                            <ion-icon name="newspaper-outline"></ion-icon>
                        </span>
                        <span class="title">Registrasi</span>
                    </a>
                </li>
                <li>
                    <a href="login.html">
                        <span class="icon">
                            <ion-icon name="log-in-outline"></ion-icon>
                        </span>
                        <span class="title">Login</span>
                    </a>
                </li>
            </ul>
        </div>

        <!-- main -->
        <div class="main">
            <div class="topbar">
                <div class="toggle">
                    <ion-icon name="menu-outline"></ion-icon>
                </div>
                <!-- search -->
                <div class="search">
                    <label>
                        <input type="text" placeholder="Search here">
                        <ion-icon name="search-circle-outline"></ion-icon>
                    </label>
                </div>
                <!--userImg-->
                <div class="user">
                    <img src="icon.jpeg">
                </div>
            </div>

            <!-- cards -->
            <!-- Diberi carousel -->
            <div class="cardBox">
                <div class="card">
                    <div>
                        <div class="numbers">150</div>
                        <div class="cardName">Daily Views</div>
                    </div>
                    <div class="iconBx">
                        <ion-icon name="eye-outline"></ion-icon>
                    </div>
                </div>
                <div class="card">
                    <div>
                        <div class="numbers">50</div>
                        <div class="cardName">Booking</div>
                    </div>
                    <div class="iconBx">
                        <ion-icon name="person-add-outline"></ion-icon>
                    </div>
                </div>
                <div class="card">
                    <div>
                        <div class="numbers">100</div>
                        <div class="cardName">Ask Question</div>
                    </div>
                    <div class="iconBx">
                        <ion-icon name="mail-unread-outline"></ion-icon>
                    </div>
                </div>
                <div class="card">
                    <div>
                        <div class="numbers">Rp 5.000.000</div>
                        <div class="cardName">Income</div>
                    </div>
                    <div class="iconBx">
                        <ion-icon name="cash-outline"></ion-icon>
                    </div>
                </div>
            </div>


            <div class="data">
                <!-- list data -->
                <div class="Orderanterbaru">
                    <div class="cardHeader">
                        <h2>Booking Terbaru</h2>
                        <a href="#" class="ABC">View All</a>
                    </div>
                    <table>
                        <form method="post">
                            <!-- 
                                Mencari data berdasarkan search bar
                                ex: tgl 30, tampilkan data bookingan tgl 30
                             -->
                            <input type="date" name="searchDate">
                            <input type="submit" name="submit" value="Cari">
                        </form>
                        <thead>
                            <tr>
                                <td>Nama</td>
                                <td>Total Harga</td>
                                <td>Status</td>
                                <td>Tanggal</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sql = "SELECT nama, harga, status, tanggal FROM customer LIMIT 5";
                            $result = $conn->query($sql);

                            if ($result->num_rows > 0) {
                                while ($row = $result->fetch_assoc()) {
                            ?>
                                    <tr>
                                        <td><?php echo $row['nama'] ?></td>
                                        <td><?php echo $row['harga'] ?></td>
                                        <td>
                                            <span class="status lunas">
                                                <?php echo $row['status'] ?>
                                            </span>
                                        </td>
                                        <td>
                                            <?php echo $row['tanggal'] ?>
                                        </td>
                                    </tr>
                            <?php
                                }
                            } else {
                                echo "Tidak ada hasil";
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="data-stats">
                <div class="stats">
                    <div class="stats-header">
                        <h2>Hitung Pemasukan dari</h2>
                        <form method="post">
                            <input type="date" name="searchFrom">
                            <input type="date" name="searchTo">
                            <input type="submit" name="submit" value="Cari">
                        </form>
                    </div>
                    <div>
                        <h2>Estimasi Pemasukan</h2>
                        <?php
                        if (isset($_POST['submit'])) {
                            $searchFrom = $_POST['searchFrom'];
                            $searchTo = $_POST['searchTo'];
                            $sql = "SELECT SUM(harga) AS total FROM customer WHERE tanggal BETWEEN '$searchFrom' AND '$searchTo' AND status='Lunas'";
                            $result = $conn->query($sql);

                            if ($result->num_rows > 0) {
                                while ($row = $result->fetch_assoc()) {
                                    echo $row['total'];
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>

    <script>
        //menutoggle
        let toggle = document.querySelector('.toggle');
        let navigasi = document.querySelector('.navigasi');
        let main = document.querySelector('.main');

        toggle.onclick = function() {
            navigasi.classList.toggle('active');
            main.classList.toggle('active');
        }
        // add hovered class in selected list item
        let list = document.querySelectorAll('.navigasi li');

        function activeLink() {
            list.forEach((item) =>
                item.classList.remove('hovered'));
            this.classList.add('hovered');
        }
        list.forEach((item) =>
            item.addEventListener('mouseover', activeLink))
    </script>
</body>

</html>