<?php
require_once "../config/connection.php";

if (isset($_POST['name']) && isset($_POST['harga']) && isset($_POST['status'])) {
  function validate($data)
  {
    return htmlspecialchars(stripslashes(trim($data)));
  }

  $name = $_POST['name'];
  $harga = $_POST['harga'];
  $status = $_POST['status'];

  $sql = "INSERT INTO customer(nama, harga, status) VALUES ('$name', '$harga', '$status')";

  if ($conn->query($sql) === true) {
    echo "Data Berhasil Ditambahkan";
  } else {
    echo "Data Gagal Ditambahkan";
  }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Register Page</title>
  <link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" />
  <link rel="stylesheet" href="style.css" />
</head>

<body>
  <div class="input">
    <h1>Tambah Data</h1>

    <form method="post">
      <div class="box-input">
        <i class="fas fa-users"></i>
        <input type="text" name="name" placeholder="Nama" />
      </div>

      <div class="box-input">
        <i class="fas fa-money-bill"></i>
        <input type="number" name="harga" placeholder="Harga" />
      </div>

      <div class="box-input">
        <i class="fas fa-tasks"></i>
        <select name="status" class="options">
          <option value="Lunas">Lunas</option>
          <option value="Belum Lunas">Belum Lunas</option>
        </select>
      </div>

      <input type="submit" value="Submit" class="btn-input" />
    </form>
  </div>
</body>

</html>