<?php
require_once "./config/connection.php";
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="customers.css" />
  <title>Customer</title>
</head>

<body>
  <main>
    <h1>List Customer</h1>
    <form method="post">
      <input type="text" name="searchBar" placeholder="Cari customer" value="<?php echo isset($_POST['searchBar']) ? $_POST['searchBar'] : '' ?>">
      <input type="submit" name="submit" value="Cari">
    </form>

    <table>
      <thead>
        <tr>
          <th>Nama</th>
          <th>Total Harga</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
        <?php
        if (isset($_POST['submit'])) {
          $keyword = $_POST['searchBar'];

          $sql = "SELECT * FROM customer WHERE nama LIKE '%$keyword%'";
          $result = $conn->query($sql);
          if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
        ?>
              <tr>
                <td><?php echo $row['nama'] ?></td>
                <td><?php echo $row['harga'] ?></td>
                <td>
                  <span class="badge">
                    <?php echo $row['status'] ?>
                  </span>
                </td>
              </tr>
            <?php
            }
          }
        } else if (!isset($_POST['submit'])) {
          $sql = "SELECT nama, harga, status FROM customer";
          $result = $conn->query($sql);

          if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
            ?>
              <tr>
                <td><?php echo $row['nama'] ?></td>
                <td><?php echo $row['harga'] ?></td>
                <td>
                  <span class="badge">
                    <?php echo $row['status'] ?>
                  </span>
                </td>
              </tr>
        <?php
            }
          } else {
            echo "Data tidak ditemukan";
          }
        } else {
          echo "Data kosong";
        }
        ?>
        <?php
        ?>
      </tbody>
    </table>
  </main>
</body>

</html>