-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.1.72-community - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for rental_ps
CREATE DATABASE IF NOT EXISTS `rental_ps` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `rental_ps`;

-- Dumping structure for table rental_ps.customer
CREATE TABLE IF NOT EXISTS `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(40) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `status` enum('Lunas','Belum Lunas') DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table rental_ps.customer: 8 rows
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` (`id`, `nama`, `harga`, `status`, `tanggal`) VALUES
	(1, 'Supri', 60000, 'Lunas', '2022-12-26'),
	(2, 'Karmin', 100000, 'Belum Lunas', '2022-12-27'),
	(3, 'Alif', 50000, 'Lunas', '2022-12-28'),
	(4, 'Fikri', 60000, 'Belum Lunas', '2022-12-29'),
	(5, 'Naufal', 80000, 'Lunas', '2022-12-30'),
	(6, 'Juliyem', 40000, 'Belum Lunas', '2022-12-31'),
	(7, 'andhika', 20000, 'Lunas', '2023-01-01'),
	(8, 'abc\r\n', 50000, 'Lunas', '2022-12-26');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;

-- Dumping structure for table rental_ps.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table rental_ps.users: 0 rows
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
